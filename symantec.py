# Copyright (c) 2023 krlplm
# Original code: https://github.com/krlplm/sitereview
# Modifications by: SpearJam (Majdi Nurhasan)

import os
import re
import sys
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from time import sleep
from dotenv import load_dotenv

# Load environment variables
load_dotenv()
CHROMEDRIVER_PATH = os.getenv("CHROMEDRIVER_PATH")

class siteReview():
    def __init__(self):
        # This is to instantiate the chrome browser, tried to hide devtools logs appearing in the commandline but somehow wouldn't
        options = webdriver.ChromeOptions()
        options.add_argument('--headless --log-level=OFF')
        options.add_experimental_option('excludeSwitches', ['enable-logging'])
        s = Service(CHROMEDRIVER_PATH)
        self.driver = webdriver.Chrome(service=s, options=options)

    def ioc_search(self, entity):
        to_ticket = ""

        # Setup for Symantec
        url = 'https://sitereview.bluecoat.com/#/'

        # Symantec Parsing
        self.driver.get(url)
        self.driver.find_element(By.ID, 'txtUrl').send_keys(entity)
        self.driver.find_element(By.ID, 'btnLookup').click()
        sleep(6) # To throttle the request

        try:
            # Parsing the sitereview response page for the element of interest
            cat = self.driver.find_element(By.XPATH, '//*[@id="submissionForm"]/span/span[1]/div/div[2]/span[1]/span')
            if self.driver.find_element(By.XPATH, '//*[@id="submissionForm"]/span/span[1]/div/div[2]/span[2]/span'):
                cat2 = self.driver.find_element(By.XPATH, '//*[@id="submissionForm"]/span/span[1]/div/div[2]/span[2]/span')
                if "Last Time" in cat2.text:
                    category = cat.text
                else:
                    category = cat.text + " | " + cat2.text
            else:
                category = cat.text
        except:
            category = 'unrated'
        
        pattern = r"\b\s+\|\s+This page was rated by our WebPulse system\b"  # Match the exact phrase and remove this part
        category = re.sub(pattern, "", category)

        if (category == "unrated"):
            to_ticket += "**Symantec**: The URL has not been rated yet." + "\nRef: " + self.driver.current_url + "\n"
        else:
            to_ticket += "**Symantec**: Category listed is " + category + ". \nRef: " + self.driver.current_url + "\n"

        sys.stdout.write(to_ticket)

if __name__ == "__main__":
    # Access passed arguments using sys.argv[1:]
    arguments = sys.argv[1:]

    bot = siteReview()
    bot.ioc_search(arguments[0])
