import os
import vt
import sys
import pyperclip
import requests
import time
from dotenv import load_dotenv

# Load environment variables
load_dotenv()
VT_API_KEY = os.getenv("API_KEY2")

class siteReview():
    def __init__(self):
        # Setup for VT
        self.client = vt.Client(VT_API_KEY)

    def ioc_search(self, entity):
        to_ticket = ""

        # VirusTotal API Request for scanning
        req_url = "https://www.virustotal.com/api/v3/urls"
        payload = { "url": entity }
        headers = {
            "accept": "application/json",
            "x-apikey": VT_API_KEY,
            "content-type": "application/x-www-form-urlencoded"
        }
        response = requests.post(req_url, data=payload, headers=headers)

        try:
            # VirusTotal API Request for getting the scan result
            req_url = ("https://www.virustotal.com/api/v3/analyses/{}".format(response.json()["data"]["id"])) # Error Marker 1
            headers = {
                "accept": "application/json",
                "x-apikey": VT_API_KEY
            }
            response = requests.get(req_url, headers=headers)

            # Polling loop with timeout
            timeout = 60  # Adjust timeout as needed (in seconds)
            start_time = time.time()

            while True:
                # Check if scan result is done after looping
                if response.json()["data"]["attributes"]["status"] == "completed":
                    # Determine if the scan is malicious or not
                    if response.json()["data"]["attributes"]["stats"]["malicious"] > 0:
                        to_ticket = ("**VirusTotal**: The score is {}/{}. Vendor(s) flagged it as malicious.\n".format(response.json()["data"]["attributes"]["stats"]["malicious"],(response.json()["data"]["attributes"]["stats"]["harmless"] + response.json()["data"]["attributes"]["stats"]["suspicious"] + response.json()["data"]["attributes"]["stats"]["undetected"] + response.json()["data"]["attributes"]["stats"]["malicious"])))
                    elif response.json()["data"]["attributes"]["stats"]["malicious"] == 0:
                        to_ticket = ("**VirusTotal**: The score is {}/{}. Report came back as clean.\n".format(response.json()["data"]["attributes"]["stats"]["malicious"],(response.json()["data"]["attributes"]["stats"]["harmless"] + response.json()["data"]["attributes"]["stats"]["suspicious"] + response.json()["data"]["attributes"]["stats"]["undetected"] + response.json()["data"]["attributes"]["stats"]["malicious"])))

                    # If additional categories are listed by vendors
                    categories = []
                    for vendor in response.json()["data"]["attributes"]["results"].values():
                        if ((vendor["category"] == "suspicious" or vendor["category"] == "malicious") and vendor["result"] != "malicious"):
                            categories.append(vendor["result"].capitalize())
                    categories = list(dict.fromkeys(categories))

                    if categories:
                        to_ticket += "Additionally, it is detected as "
                        if len(categories) > 1:
                            for category_vt in categories:
                                if category_vt == categories[-1]:
                                    to_ticket += ("and **{}**.\n".format(category_vt))
                                else:
                                    to_ticket += ("**{}**, ".format(category_vt))
                        else:
                            to_ticket += ("**{}**.\n".format(categories[0]))

                    to_ticket += "Ref: https://www.virustotal.com/gui/url/" + response.json()["meta"]["url_info"]["id"] + "\n\n"
                    break

                # Check for timeout
                elapsed_time = time.time() - start_time
                if elapsed_time > timeout:
                    to_ticket += "**VirusTotal:** Scan timed out after", timeout, "seconds."
                    break  # Handle timeout scenario

                time.sleep(2)  # Adjust polling interval as needed
                response = requests.get(req_url, headers=headers) # Get latest scan result
        except:
            to_ticket += "**VirusTotal**: The URL could not be scanned due to error {}. \nError Message - *{}*\n\n".format(response.json()["error"]["code"], response.json()["error"]["message"])

        sys.stdout.write(to_ticket)

    def file_ioc_search(self, entity):
        # Opening the submitted text file
        with open(entity, 'r') as file:
            lines = file.readlines()
        lines = [line.strip() for line in lines]

        to_ticket = "**VirusTotal Analysis for Alerted File(s)**\n\n"

        # Check each file hash and send a get request using the vt-py's API library
        for line in lines:
            try:
                to_ticket += ("**Scan Result for {}:**\n".format(line))
                scan_result = self.client.get_object("/files/" + line)

                # Parsing the file information
                to_ticket += ("The score is {}/{}. This file was last analyzed on {}.\n".format(scan_result.last_analysis_stats["malicious"], (scan_result.last_analysis_stats["harmless"] + scan_result.last_analysis_stats["suspicious"] + scan_result.last_analysis_stats["undetected"] + scan_result.last_analysis_stats["malicious"]), scan_result.last_analysis_date))
                to_ticket += ("It's most interesting file name it goes under is **{}**.\n".format(scan_result.meaningful_name))
                to_ticket += ("Lastly, it has the following tags: {}.\n".format(scan_result.tags))
                to_ticket += ("Ref: https://www.virustotal.com/gui/file/{}/detection\n\n".format(scan_result.sha256))
            except:
                to_ticket += ("No matches found in VirusTotal.\n")
                to_ticket += ("Ref: https://www.virustotal.com/gui/search/{}\n\n".format(line))
        
        print(to_ticket)
        pyperclip.copy(to_ticket)

        self.client.close()

if __name__ == "__main__":
    # Access passed arguments using sys.argv[1:]
    arguments = sys.argv[1:]

    bot = siteReview()
    bot.ioc_search(arguments[0])