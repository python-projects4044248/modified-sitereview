import json
import requests
import os
import webbrowser
import sys
import time
from dotenv import load_dotenv

# Load environment variables
load_dotenv()
URLSCAN_API_KEY = os.getenv("API_KEY1")

class siteReview():
    def ioc_search(self, entity):
        to_ticket = ""

        # Setup for URLScan
        headers = {'API-Key':URLSCAN_API_KEY,'Content-Type':'application/json'}
        data = {"url": entity, "visibility": "private"}
        response = requests.post('https://urlscan.io/api/v1/scan/', headers=headers, data=json.dumps(data))

        try:
            # Polling loop with timeout
            scan_id = response.json()["uuid"] # Error Marker 1
            timeout = 60  # Adjust timeout as needed (in seconds)
            start_time = time.time()

            while True:
                # Check scan status after a short interval
                result_url = f"https://urlscan.io/api/v1/result/{scan_id}"
                result_response = requests.get(result_url)
                
                try:
                    # URLScan Parsing
                    if (result_response.json()["verdicts"]["overall"]["malicious"] == False): # Error Marker 2
                        to_ticket += "**URLScan**: No verdicts found, URL is not detected as malicious.\n"
                    else:
                        if (result_response.json()["verdicts"]["overall"]["brands"] and result_response.json()["verdicts"]["overall"]["categories"][0] == "phishing"):
                            to_ticket += "**URLScan**: URL is detected as a " + result_response.json()["verdicts"]["overall"]["brands"][0].capitalize() + " phishing site.\n"
                        else:
                            to_ticket += "**URLScan**: URL is detected as malicious.\n"
                    
                    # Check if there is an automatic downloaded file when URL is visited
                    if ("download" in result_response.json()["meta"]["processors"]):
                            to_ticket += "Additionally, a file (**" + result_response.json()["meta"]["processors"]["download"]["data"][0]["filename"] + "** | SHA256 Hash: " + result_response.json()["meta"]["processors"]["download"]["data"][0]["sha256"] + ") is automatically downloaded when the URL is visited.\n"

                    # Accessing the URLScan screenshot and opening with firefox
                    urlscan_result_url_pic = ("https://urlscan.io/screenshots/" + response.json()["uuid"] + ".png") 
                    firefox = webbrowser.Mozilla("C:\\Program Files\\Mozilla Firefox\\firefox.exe") 
                    firefox.open(urlscan_result_url_pic) 

                    to_ticket += "Ref: " + response.json()["result"] + "\n\n"
                    break 
                except:
                    string_for_testing = "Error {}. \nError Message - *{}*\n\n".format(result_response.json()["status"], result_response.json()["message"])

                # Check for timeout
                elapsed_time = time.time() - start_time
                if elapsed_time > timeout:
                    to_ticket += "**URLScan:** Scan timed out after", timeout, "seconds."
                    break  # Handle timeout scenario

                time.sleep(2)  # Adjust polling interval as needed
        except:
            to_ticket += "**URLScan**: The URL could not be scanned due to error {}. \nError Message - *{}*\n\n".format(response.json()["status"], response.json()["errors"][0]["detail"])

        sys.stdout.write(to_ticket)

if __name__ == "__main__":
    # Access passed arguments using sys.argv[1:]
    arguments = sys.argv[1:]

    bot = siteReview()
    bot.ioc_search(arguments[0])
