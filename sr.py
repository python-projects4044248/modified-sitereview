# Copyright (c) 2023 krlplm
# Original code: https://github.com/krlplm/sitereview
# Modifications by: SpearJam (Majdi Nurhasan)

from argparse import ArgumentParser
from tkinter import *
import tkinter.messagebox 
import pyperclip
import virustotal
import multiprocessing
import subprocess
import time

# Function to run multiple scripts
def run_script(script_path, data):
    # subprocess.run(["python", script_path] + list(args))  # Pass arguments to the script (Previous one used)

    process = subprocess.Popen(["python", script_path, data],
                                stdout=subprocess.PIPE,
                                stderr=subprocess.DEVNULL)  # Discard stderr
    output, _ = process.communicate()
    return output.decode('utf-8').strip()  # Decode and remove trailing characters

# Function to show an information messagebox
def show_message(message, title):
    tkinter.messagebox.showinfo(message=message, title=title)

# Function to process single domain/IP Address
def cmd_parse(cmd):
    entity = cmd
    scripts = ["symantec.py", "urlscan.py", "virustotal.py"]
    urls = [entity, entity, entity]

    pool = multiprocessing.Pool(processes=3)  # Replace n with desired number of processes
    results = pool.starmap(run_script, zip(scripts, urls))  # Combine script paths and URLs

    # Process results from different script files and paste to clipboard
    print("\nScan Results:\n") # For a cleaner output in terminal
    to_ticket = ""
    for result in results:
        result = result.replace("\r", "")
        to_ticket += result + "\n\n"
    
    print(to_ticket)
    pyperclip.copy(to_ticket)

# Function to process a text file containing file hash(es)
def file_parse(file):
    bot = virustotal.siteReview()
    bot.file_ioc_search(file)

def main():
    start_time = time.time()

    p = ArgumentParser()
    p.add_argument("-c", "--cmd", type=str, help="Enter the single domain/IP")
    p.add_argument("-f", "--file", type=str, help="Get VT findings for file hashes listed in hashes.txt")
    
    args = p.parse_args()

    if args.cmd:
        print('\nSitereview: {}'.format(args.cmd))
        cmd_parse(args.cmd)
        print("---\nThe output is automatically copied to your clipboard.")
    elif args.file:
        print('\nSitereview: {}\n'.format(args.file))
        file_parse(args.file)
        print("---\nThe output is automatically copied to your clipboard.")
    else:
        print("\n" + "Note: Please supplement the single domain/IP by using switch -c or a list of file hashes with the file path by using switch -f" + "\n")
    
    elapsed_time = time.time() - start_time
    show_message("The script is done running.\nElapsed Time: {:.2f} seconds".format(elapsed_time), "Information")

if __name__ == "__main__":
    main()
else:
    pass