import re
import os

print(":. API Keys and Chromedriver File Path Setup .: \n\n")

# Input API Keys and chrome driver path
urlscan_api_key = input("Enter your URLScan API Key (32 character key with dashes): \n")
vt_api_key = input("\nEnter your VirusTotal API Key (64 character key without dashes): \n")
chromedriver_path = input("\nEnter the path for the downloaded chromedriver (Ex: D:\Folder1\Folder2\chromedriver.exe): \n")
chromedriver_path = re.sub(r"\\", r"\\\\", chromedriver_path)

# Create .env file if it does not exist
if not os.path.exists(".env"):
    with open(".env", "w") as file:
        file.write("API_KEY1=KEY #urlscan_key_marker\n")
        file.write("API_KEY2=KEY #vt_key_marker\n")
        file.write("CHROMEDRIVER_PATH=PATH #path_marker\n")

# Editing .env
with open(".env", "r") as file:
    lines = file.readlines()

urlscan_linenumber = 1
vt_linenumber = 1
path_linenumber = 1
linenumber = 1
for line in lines:
    if "#urlscan_key_marker" in line:
        urlscan_linenumber = linenumber
    elif "#vt_key_marker" in line:
        vt_linenumber = linenumber
    elif "#path_marker" in line:
        path_linenumber = linenumber
    elif ((urlscan_linenumber > 1) and (vt_linenumber > 1) and (path_linenumber > 1)):
        break
    linenumber += 1

lines[urlscan_linenumber - 1] = "API_KEY1={} #urlscan_key_marker\n".format(urlscan_api_key)
lines[vt_linenumber - 1] = "API_KEY2={} #vt_key_marker\n".format(vt_api_key)
lines[path_linenumber - 1] = "CHROMEDRIVER_PATH={} #path_marker\n".format(chromedriver_path)

with open(".env", "w") as file:
    file.writelines(lines)

# Files have been modified
print("\nThe .env file has been created/modified. Make sure the dependencies and pre-requisites are installed before running sitereview.py!")
