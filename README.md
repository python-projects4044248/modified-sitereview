# Site Review
Site Review Checker (Modified by SpearJam)

## Description

This is a modification of krlplm's work (https://github.com/krlplm/sitereview) on sitereview which adds VirusTotal and URLScan as additional OSINT to be used besides Symantec.  

However do note this is still a work in-progress, it's main functionality is to print out the results in a CLI (and clipboard) using the markdown format.  

Printing results to textfiles, and scanning several URLs at once with different OSINTs are the next goals for this project.

**03/12/2024**: New function added, can now pull previous VT scan results from file hashes.

**03/14/2024**: Overhauled sitereview.py and remade it into sr.py as I separated the code logic per OSINT, utilized subprocess and multiprocessing to process the API calls separately (which makes it faster, at least -20 seconds when compared previously), and of course renamed the file for ease of use.

## Setup

1. Make sure the pre-requisites & package dependencies below are installed & downloaded (and the accounts for the respective platforms are created).
2. Clone or download the repository files to any directory on your local computer.
3. Run helper.py (enter "python helper.py" in command line) to create a .env file and insert the API keys and chromedriver path there.
4. Run sitereview.py following the format as mentioned in the **Usage** section below.

When updating:
Do the following git commands below,
```
git fetch --all
git reset --hard origin/main
```

OR manually download the files and overwrite them in the directory you placed your previous files.

Then make sure to run helper.py as needed if your .env file was deleted/missing or you changed your API keys/Chromedriver file path.

## Pre-requisites

- Python 3.7+
- Firefox (This is the default browser set in the code to open the URLScan Screenshot)
- Download Chrome Driver compatible with the Browser version you run from https://chromedriver.chromium.org/downloads
  * For chrome versions 115 and newer, you will have to download the drivers from https://googlechromelabs.github.io/chrome-for-testing/#stable
  * Place the chromedriver.exe file in any directory as you wish, but it is recommended to put it inside the script folder for ease of use.

VirusTotal: 
- VirusTotal account for API usage

URLScan:
- URLScan account for API usage (Make sure to set the visibility on your profile to what you need, ex: private, public, etc.)

## Package Dependencies

Install the dependencies by running the commands below separately,  
```
pip install selenium
pip install pyperclip
pip install vt-py
pip install python-dotenv
```

Other package dependencies are available by default from the official python installation.

## Usage

sitereview.py takes either of the arguments, c/cmd OR f/file.

```
How to Run:
sitereview.py [-c] [-f]

Arguments:
  -c cmd, --cmd <domain/ip>       Submit single domain/ip/url to Blue Coat's Site Review
  -f file, --file <path_to_file>  Include the list of file hashes separated by a new line specifying the absolute path
  
Examples:
- python sitereview.py -c google.com
- python sitereview.py -c 8.8.8.8
- python sitereview.py -c https://www.test.com
- python sitereview.py -f hashes.txt

```
## Results
- The results are shown in the console and copied automatically to the clipboard with the help of pyperclip.

## Known Issues

1. result.webdriverValue.value is missing or empty in Runtime.callFunctionOn response (Session info: headless chrome=XXX)  
**Fix**: Download latest chromewebdriver (Make sure your Chrome version and chromedriver version are compatible with each other).
2. (Harmless) When using the check file hash function (-f), the script delivers the error message "Python has stopped working" even though the script runs fully and the output is not tampered. Possible cause is the usage of the vt-py python dependency.  
**Fix**: Currently looking into this, however it currently serves as a nice notification that tells you that the script is finished running.

## Acknowledgments

This project is based on the `sitereview` repository developed by krlplm (https://github.com/krlplm/sitereview). I would like to express my gratitude to krlplm for their contributions to this project.

My modifications to the original code include:
- Formatting results to a markdown format both in command line and directly thru clipboard after analysis.
- Making the selenium driver use a headless chrome browser.
- Added VirusTotal and URLScan as additional site checkers besides Symantec.
- Automatically opening the URLScan result screenshot after analysis.
- Added further VT Scan functionality by having the ability to get previous scan results from file hashes.
- Separated the code logic per OSINT, and used subprocess & multiprocessing to run parallel processes per OSINT script to make it faster (but also looking to use asyncio as an alternative).

